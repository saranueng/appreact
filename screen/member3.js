import React from 'react';
import { ScrollView,Image, StyleSheet, Text, View } from 'react-native';
import HeaderBar from '../components/header';
import Card from '../components/card'

export default class Member3Screen extends React.Component {
  render() {
    let pic3 = { uri: 'https://cdn.images.express.co.uk/img/dynamic/36/590x/Aquaman-movie-trailer-news-918327.jpg' }

    return (
      <ScrollView>
        <Card img={pic3} />
        <Text style={{fontSize: 20 , fontWeight: 'bold'}}>== MEMBER 3 ==</Text>
        <View style={{alignItems: 'center' , fontWeight: 'bold' , padding: 10 }}>
        <Text>Name : Mr.Sarit Thongsangiam</Text>
        <Text>Nickname : Tao</Text>
        <Text>ID : 6031305058</Text>
        <Text>Adress : Nakhon Pathom</Text>
        <Text>School : IT</Text>
        <Text>Mejor: SE</Text>
        </View>
      </ScrollView>
    );
  }
}


