import React from 'react';
import { ScrollView, Image, StyleSheet, Text, View } from 'react-native';
import HeaderBar from '../components/header';
import Card from '../components/card'
import { TouchableOpacity } from 'react-native-gesture-handler';

export default class HomeScreen extends React.Component {
  render() {
    let pic1 = { uri: 'https://www.img.in.th/images/e72b6ebd1762ac04fc1e7007a2339648.jpg' }
    let pic2 = { uri: 'https://i.ytimg.com/vi/gRXLwz_Fvmw/maxresdefault.jpg' }
    let pic3 = { uri: 'https://cdn.images.express.co.uk/img/dynamic/36/590x/Aquaman-movie-trailer-news-918327.jpg' }

    return (
      <ScrollView>
        <HeaderBar headtitle='My App' />
        <TouchableOpacity
          onPress={() => this.props.navigation.navigate('Member1')}>
          <Card img={pic1} title='Sarayuth' />
        </TouchableOpacity>
        <TouchableOpacity onPress={() => this.props.navigation.navigate('Member2')}>
        <Card img={pic2} title='Peeranut'/>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => this.props.navigation.navigate('Member3')}>
        <Card img={pic3} title='Sarit'/>
        </TouchableOpacity>
      </ScrollView>
    );
  }
}


