import React from 'react';
import { Text, View } from 'react-native';

export default class HeaderBar extends React.Component {
    render() {
        return (
            <View style={{
                marginTop: 0, 
                backgroundColor: '#F0FFFF',
            }}>
                <Text style={{
                    fontSize: 30,
                    textAlign: 'center',
                    padding: 5
                }}>
                    {this.props.headtitle}
                </Text>
            </View>
        );
    }
}


